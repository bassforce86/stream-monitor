+++
title = "Home"
tags = ["stream-monitor", "chrome-extension"]
description = "Stream Monitor Website Home Page"
+++

{{< image position="center" src="img/sample.png" title="Sample Image" >}}

Welcome to the site everyone!

Stream Monitor aims to show some basic information, in a meaningful but unobtrusive way.

Currently, we show:
- Resolution.
- Frames per Second. 
- Frame Drops per Second.
- Current Browser Time. 

*(Please note, this uses the time according to your browser, which may not be totally accurate)*