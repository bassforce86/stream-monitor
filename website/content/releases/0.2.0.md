+++
title = "0.2.0"
date = "2020-06-13"
author = "James"
description = "Mainly Support for Youtube & Twitch videos & this Website release!"
tags = ["0.2.0", "stream monitor"]
+++

### Features

- **#24:** setup basic pages website
- **#11:** Added support for twitch & youtube

### Fixes

- **#29:** footer layout and styling
- **#29:** base url updated
- **#29:** styles missing from site
- **#29:** added website, discord and donations links to popup 