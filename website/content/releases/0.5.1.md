+++
title = "0.5.1"
date = 2020-07-31T17:17:46+01:00
author = "James"
description = "0.5.1"
tags = ["0.5.1", "stream monitor"]
+++

## Fixes
- **#47:** added a fix for the Resolution displaying incorrectly for some elements