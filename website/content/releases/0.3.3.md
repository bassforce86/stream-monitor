+++
title = "0.3.3"
date = "2020-06-25T20:57:54+01:00"
author = "James"
description = "Fixed the installation launch issue"
tags = ["0.3.3", "stream monitor"]
+++

 ## Fixes

 - **#43:** fixed launch failures (hopefully?)
