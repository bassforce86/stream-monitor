+++
title = "About"
date = "2020-06-11"
author = "James"
+++

# Hi there

My Name is James, I'm the main developer on Stream Monitor. 

This little project was born from the idea of Stadia+, but aimed to have smaller goals.
The main idea behind this Chrome Extension is to not impact or interact with the stream directly. 
Instead, we poll the information after it's been relayed to the site. 

So whilst the information may be fractionally inaccurate, that fraction means that we can be sure we're not interrupting your stream in any way. Whether you're streaming to somewhere, or watching something live, this extension will not cause you any delays seeing your frames rendered to the screen.
