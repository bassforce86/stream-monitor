# Simple Web Streaming Monitor

Stream Monitor was originally designed as a simply way for me to see what my Stadia stream was doing. 

Since then, from the ideas of others it has become a more general streaming monitor for use with Stadia & Twitch. (others will follow soon...)

### Current features
- Resolution
- Frame Rate (per second)
- Frame Drops (per second)

## Development Contribution Guidelines

- Do NOT include libraries unless absolutely required.
- This project uses **[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/#summary)** to aid with management. Please follow this to the best of your ability.
- The Project should maintain current best practice as much as possible.
- Comment your code, all developers know things, some more than others. Comments help others to learn, as well as helping reviewers to understand the problem being addressed

## Local Development Guide

- Setup your project folder
- open a terminal/command prompt
- navigate to your project folder
- run `git clone https://gitlab.com/bassforce86/stream-monitor.git`
- Open Chrome and go to: chrome://extensions/
- enabled Developer mode
- click `Load unpacked extension`
- point it towards `<project folder>/stream-monitor`
- load up any supported page and you should see the monitor come to life ;)

## Maintainers
- James - @bassforce86
- Martin - @Marchief

## Donations 

Helping out the project by donating is always very welcome! Keeps us motivated to continue improving and adding new features!

You can donate [Here](https://ko-fi.com/bassforce86)!