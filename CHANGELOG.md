# CHANGELOG

<!--- next entry here -->

## 1.3.2
2024-06-08

### Fixes

- include netflix as trusted source (ef19327cf26845dc347997bd1b632650a387d534)
- update patch ntoes for 1.3.1 and 1.3.2 (343acc1b5e7533a0b3777e10c9755bebd127092c)

## 1.3.1
2022-12-24

### Fixes

- update with v1.3.0 info (55dfe8a181bc2aa5e5d7b1ded427a08929607764)
- theme update (84e23c6e98b63d75deb33586f2c2eb2aa464faa0)
- update to use main as default branch (dd2520765ae7e568e963f287dc99706895d5076e)
- update to extended image (91e72ab7fc33de35a70ac68ec97c4bf918c8c311)

## 1.3.0
2022-08-30

### Features

- removed data monitor (673580039d73adc9879c5c75237b7078f6110b8f)

### Fixes

- minor website update (37c187f0a7bc0106f48e538c3f4edbbf15bf3650)
- updated to chrome manifest v3 (cd52e71fcc94ea5d99ace7304f05a4b9f05dde4f)

## 1.2.0
2020-09-07

### Features

- **#52:** added browser time display (74062eb990d836b581f2ab853a2f34aa272b0df4)

## 1.1.1
2020-09-04

### Fixes

- **#54:** dropped debug level for live app. (2bde70799b838766ba227eb92b38856bf59759a2)

## 1.1.0
2020-08-04

### Features

- **#50:** add hide_all option (1bb8a35bc7b90954c10e4120a66d379ae21bf238)

### Fixes

- updated website theme (3c34dface7c2c702a6995c5fb68dacdb09c8338e)
- **#51:** updated load state to include data_usage monitor (dcca2ca820312dec7384093a5dbe03179aff46c4)
- **#50:** removed height styles (5ee7c1f27d3bba987e1c83af9087638dc8569b46)
- **#50:** ensure hide/show don't update position (05c6a8af5f77de3159ae7dec564e16c297e82087)
- **#50:** caught potential position error (15d701a8a6d9baef6165f04f6f56ca0bd5b1dd92)

## 1.0.1
2020-08-03

### Fixes

- added commit changelog back into pipline (09659a7069b27394f2260d5853114cdffba60b25)
- **#49:** started implementation for Stadia (14d70cb7dab8345610f5facb3a397b0f047b984f)
- moved body insert to last in list (a5e71813cbd50197c96f95ec3ccb505132d84772)
- fixes still in progress (1e10158a03f50bf2f4fb79c32111d2602f0090ca)
- **#49:** Stadia Data now registering correctly. (e764098a13098f7eb0805a7532bceb5d0deeea73)
- removed redundant ontrack notification & variable naming (68193e19ac49ad4af3759e5e80082dcab2673751)

## 0.5.1
2020-07-31

### Fixes

- {#47): validated height & width together (92bdb9cbfc5a448f31c07cb305ac67812deb2734)

## 0.5.0
2020-07-30

### Features

- **#4:** added Data Usage monitor (06e319f72e5330cbd4693e2b0caaf04e8dc6a1f9)

### Fixes

- **#4:** updated naming standard for readability (471fea69c160c5a3e652c13e024285931292378a)

## 0.4.7
2020-07-30

### Fixes

- removed custom options view - not required for fix. (d0ff2f1f28f72a9083e29549d14570dc6d4245d8)
- minor refactor (2a7650819a6ef0917f096631dc711df76c82ef76)
- started initial refactor (ff8f4bcfc9fa406ef94d166ff8752cdecc029dbe)

## 0.4.6
2020-07-30

### Fixes

- **#46:** dropped minmum video detection to 114p (0f7010429281648f7561e5ff81fcdbee3d672ac4)

## 0.4.5
2020-07-30

### Fixes

- **#27:** included more information (1f50e0efdf1a6c924693f763179f5a89472f97ad)
- **#45:** removed 'tabs' permission (9d70ad24ce1622328ca69f77594c2826d7be82df)

## 0.4.4
2020-07-03

### Fixes

- **#27:** included more information (d6b2274917e7df13acfe6b06e68b54d4f25f7d18)

## 0.4.3
2020-07-02

### Fixes

- version bump (56cbf5b0d60bd5d295b2a5c11abfc2fe8fa60437)

## 0.4.2
2020-07-02

### Fixes

- updated icons (b49f3d9dd2d2d4475da591bd868291e27183accc)
- updating manifest values (97faca4cfd6c02c7e649ab11ed5118f76dc89197)
- added omnibox (8a72d5c4638da4e04cb267bad3a18e89e4b64202)

## 0.4.1
2020-07-02

### Fixes

- **#44:** fixed position state on popup load (05478d4a3d6203f6b174aa985ea5db5dc6fefbe3)
- version bump (1e7b2dd41e5d94549fe5256d1817345c5f6cb8b6)

## 0.4.0
2020-06-27

### Features

- **#33:** added more position options (685c4eb821b773f9104143e1f79ae0620df824b0)

## 0.3.5
2020-06-25

### Fixes

- **#43:** added defaults on install (1e4da7bddfb1398f984687979c2cf0cb8fa9b75e)

## 0.3.4
2020-06-25

### Fixes

- 0.3.3 release description updated (5c498d8d6d24f0fb9d523747f0edf588590388a6)

## 0.3.3
2020-06-25

### Fixes

- **#43:** removed old failing code (7649a54039db0280229af688e4777fe47c0fd382)

## 0.3.2
2020-06-25

### Fixes

- version notes (fcbd0576d02350214ee2caeb4ad8efc464058336)

## 0.3.1
2020-06-24

### Fixes

- update release notes (4f06b0e027076a1f5773e4218150afa9aa74d89e)

## 0.3.0
2020-06-24

### Features

- **#41:** remove cookies. (01c204c86e9cbba515c8ff7fd9f37eb6b040bb62)

## 0.2.5
2020-06-20

### Fixes

- **#38:** menu links now open (d7a295e869bfaf5a84e0583974e7c1c23b665f69)

## 0.2.4
2020-06-20

### Fixes

- **#36:** clicks now registered through monitor (fedd4c7bbdf02ef3f66204e35366b884bb47ea44)

## 0.2.3
2020-06-15

### Fixes

- **#32:** fixed font scaling across supported sites (4c2a15a6b91905718f5b86414222373d71ecc99e)

## 0.2.2
2020-06-14

### Fixes

- **#31:** Monitor options saved between sites (c7ce2ccff40e2622d13483398b753e50114252b7)

## 0.2.1
2020-06-13

### Fixes

- **#30:** extension popup now shows correctly. (ef01756687cf0605ae3e7e09225c1e18cdab1e06)

## 0.2.0
2020-06-13

### Features

- **#24:** setup basic pages website (259ffed11c1921ba0f038ea676eb7af34718e07d)
- **#11:** Added support for twitch & youtube (123159728896fe071c65829c0cbefc0ce4631163)

### Fixes

- footer layout and styling (3faa55a19e0fbcffffda5210c8658f3f2a60c1e7)
- **#29:** base url updated (bf2d62dbfa93ad68a90372b01280705c25d192e6)
- styles missing from site (5f09654f987554f1ce6d1cd2798c52af8e59cb7b)
- added website, discord and donations links to popup (3d619ff9a9466938d5f4624fc9f6a986376703df)

## 0.1.1
2020-06-11

### Fixes

- **#25:** frame rate and drop now updating (9e54f100d7d96b8f8274b693e04dbc7726ce027c)

## 0.1.0
2020-06-10

### Features

- **#2:** complete refactor (f4a0110150baa163fb0918eb8816e8aa020b2bbe)
- **#6:** added frame drops per second (79c92e634b44c006146b9fdd1c8429e23d8ad69e)
- **#7:** added config options (708dc13f94de59652266737f9bea54d1efacde08)
- **#1:** pipelines now working (3928e659981726d76a7538dc96f477c2d299ff6e)

### Fixes

- monitor_position caused errors - non-functional (09da407fdebba894fc266595ea194e718057fb9e)
- updated res for more accurate display (e7bbd4f7dc895911d5eef62a4995f6326975ef91)
- **#8:** added stream loop to fps (3ef1827a9dda4e93b544a34e530bf336cc31ecc2)
- resolution now working after refactor (26be80185e70588bf3df5d3b815823f6168386f8)
- **#10:** added css and broke styles out (c87fd533f7a273934ea88acf5ce689e0260cfbc6)
- **#10:** added radius var and re-ordered monitors (745ea6c776332567453e9889dadca7a3deb59f8f)
- **#15:** updated references to Stadia (3d7d0ed6370277799528bf6754ebddb4125198d7)

## 0.1.0-1-add-ci-cd-for-pipeline-builds-and-release-strategies.1
2020-06-10

### Features

- **#2:** complete refactor (f4a0110150baa163fb0918eb8816e8aa020b2bbe)
- **#6:** added frame drops per second (79c92e634b44c006146b9fdd1c8429e23d8ad69e)
- **#7:** added config options (708dc13f94de59652266737f9bea54d1efacde08)
- **#1:** starting pipeline work (07625541e6dd894d0bf2e4e48f5b7871a499ec36)

### Fixes

- monitor_position caused errors - non-functional (09da407fdebba894fc266595ea194e718057fb9e)
- updated res for more accurate display (e7bbd4f7dc895911d5eef62a4995f6326975ef91)
- **#8:** added stream loop to fps (3ef1827a9dda4e93b544a34e530bf336cc31ecc2)
- resolution now working after refactor (26be80185e70588bf3df5d3b815823f6168386f8)
- **#10:** added css and broke styles out (c87fd533f7a273934ea88acf5ce689e0260cfbc6)
- **#10:** added radius var and re-ordered monitors (745ea6c776332567453e9889dadca7a3deb59f8f)
- **#15:** updated references to Stadia (3d7d0ed6370277799528bf6754ebddb4125198d7)