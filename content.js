'use strict';

let timestamp = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '');
// Inject the script tag programmatically so we can make use of ES6 modules
const monitor = document.createElement('script');
monitor.setAttribute("type", "module");
monitor.setAttribute("src", chrome.runtime.getURL('src/main.js'));

const DebugLevel = 2;

// We have to pass a message from the background via the content js 
// so that it understands the page context.
chrome.runtime.onMessage.addListener((message, sender, callback) => {
    if (DebugLevel < 2) { console.info(`%c[${timestamp}][received][message]`, 'color: #aed581;', message); }
    
    if (message.from == 'background') {
        if (message.subject == 'settings') {

            if (DebugLevel < 2) { console.info(`%c[${timestamp}][sent][event]`, 'color: #81d4fa;', message.settings); }
            window.dispatchEvent(new CustomEvent('stream_monitor_load_settings', 
                {detail: {options: message.settings}}
            ));
            callback({success: true, data: message});
        }
        if (message.subject == 'update') {

            if (DebugLevel < 2) { console.info(`%c[${timestamp}][sent][event]`, 'color: #81d4fa;', message.updates); }
            window.dispatchEvent(new CustomEvent('stream_monitor_update', 
                {detail: {options: message.updates}}
            ));
            callback({success: true, data: message});
        }
    }
    callback({success: false, data: chrome.runtime.lastError});
});

window.addEventListener('stream_monitor_settings_sync', function(event){
    if (DebugLevel < 2) { 
        console.info(`%c[${timestamp}][received][event]`, 'color: #aed581;', {
            event: 'stream_monitor_settings_sync',
            data: event.detail
        });
    }

    chrome.runtime.sendMessage({
        from: 'content',
        subject: 'settings',
        settings: event.detail
    });
});

// append the script tag to the document body so it doesn't slow down the page load.
const body = document.body || document.getElementsByTagName("body")[0];
body.insertBefore(monitor, body.lastChild);
